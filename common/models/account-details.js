module.exports = function(accountDetails) {
accountDetails.validatesUniquenessOf('accountNumber', {message: 'Account Number could be unique'});
  accountDetails.observe('before save', function (ctx, next) {
    if (ctx.instance != undefined && ctx.instance != null) {
      ctx.instance.accountNumber=(ctx.instance.accountNumber.toLowerCase());
      ctx.instance.createdTime = new Date();
      next();
    } else {
      if(ctx.data.accountNumber!=undefined && ctx.data.accountNumber!=null && ctx.data.accountNumber!=''){
        ctx.data.accountNumber=(ctx.data.accountNumber.toLowerCase());
      }
      ctx.data.updatedTime = new Date();
      next();
    }
  });
};

