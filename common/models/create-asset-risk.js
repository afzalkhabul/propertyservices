var server = require('../../server/server');
module.exports = function(CreateAssetRisk) {
CreateAssetRisk.observe('loaded', function(ctx, next) {
    if(ctx.instance){
      var department = server.models.AssetDepartments;
      var departmentId=ctx.instance.department;
      if(departmentId!=null && departmentId!=undefined){
         department.find({'where':{'id':departmentId}}, function (err, departmentList) {
           if(departmentList!=null && departmentList.length>0){
              var depName=departmentList[0].name;
            ctx.instance['departmentName']=depName;
            }
              next();
          });
      }else{
        next();
      }
      }
    else{
      next();
    }
  });
};
