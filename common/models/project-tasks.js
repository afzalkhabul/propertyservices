var server = require('../../server/server');
module.exports = function(Projecttasks) {
  Projecttasks.observe('before save', function (ctx, next) {

  if (ctx.instance != undefined && ctx.instance != null) {

      Projecttasks.findOne({'where':{'and':[{'headerId':ctx.instance.headerId},{'planId':ctx.instance.planId}]}},function (err, projectTasks) {
        if(projectTasks!=undefined && projectTasks!=null){
          var taskList=projectTasks.taskList;
          if(ctx.instance.taskList!=undefined && ctx.instance.taskList!=null && ctx.instance.taskList.length>0){
            var findStatus=false;
            for(var i=0;i<ctx.instance.taskList.length;i++){
            if(taskList.length>0){
              for(var x=0;x<taskList.length;x++){
                var taskData=taskList[x];
                 if(taskData.subTaskName.toLowerCase()==ctx.instance.taskList[i].subTaskName.toLowerCase()){
                    findStatus=true;
                    break;
                  }
                }

              }
              if(findStatus){
                break;
              }else{
                taskList.push(ctx.instance.taskList[i]);
              }
            }
            if(findStatus){
              var error = new Error('Duplicate name in Subtask');
              error.statusCode = 402;
              next(error);
            }else{
              var data={
                'taskList':taskList,
                'updatedTime' : new Date()
              };
              projectTasks.updateAttributes(data,function (err, updatedProjectTask) {
              });
              var error = new Error('Inserted successfully');
              error.statusCode = 200;
              next(error);
            }
          }
        }else{
            ctx.instance.createdTime = new Date();
            next();
        }
      });
    } else {
      ctx.data.updatedTime = new Date();
      next();
    }
  });

  Projecttasks.observe('loaded', function(ctx, next) {
    var ProjectHeader=server.models.ProjectHeader;
    var Employee=server.models.Employee;
    if(ctx.instance) {
      if (ctx.instance.headerId) {
        ProjectHeader.findById(ctx.instance.headerId, function (err, headerInfo) {
          if (err) {
            //next(err, null);
          } else {
            ctx.instance.name = headerInfo.name;

          }

          if(ctx.instance.taskList.length > 0){
            function getResourceName(i){
              if(ctx.instance.taskList[i].resourceId){
                Employee.findById(ctx.instance.taskList[i].resourceId, function(err, empData){
                  if(err){

                  }else {
                    ctx.instance.taskList[i]['resource'] = empData.name;
                  }
                  if(i+1 == ctx.instance.taskList.length){
                    next();
                  }else{
                    getResourceName(i+1);
                  }
                });
              }else{
                if(i+1 == ctx.instance.taskList.length){
                  next();
                }else{
                  getResourceName(i+1);
                }
              }
            }
            getResourceName(0);
          }else{
            next();
          }
        });
      }else{
        next();
      }
    }else{
      next();
    }
  });


  Projecttasks.getDetails = function (planId, cb) {

    Projecttasks.find({'where':{"planId": planId}},function(err, taskDetails){
      if(taskDetails!=undefined && taskDetails!=null && taskDetails.length>0){
        var finalList=[];
      for(var i=0;i<taskDetails.length;i++){
        var data=taskDetails[i];
        if(data.headerId!=undefined && data.headerId!=null && data.headerId!=''){
          if(data.taskList!=undefined && data.taskList!=null && data.taskList.length>0 ){
           var finalObject= {
             'headerName': data.name,
             'noOfTasks': data.taskList.length
           }
            var totalBudget=0;
            var utilizedBudget=0;
            var completedPercentage=0;
            var startDate=new Date();
            var endDate=new Date();
            var slipDates;
            for(var j=0;j<data.taskList.length;j++){
              if(j==0){
                var parts = data.taskList[j].startDate.split("-");
                startDate=new Date(parts[2], parts[1] - 1, parts[0]);
                parts = data.taskList[j].endDate.split("-");
                endDate=new Date(parts[2], parts[1] - 1, parts[0]);
                totalBudget=totalBudget+parseFloat(data.taskList[j].estimatedCost);
                utilizedBudget=utilizedBudget+parseFloat(data.taskList[j].utilizedCost);
              }
              else{
                var parts = data.taskList[j].startDate.split("-");
                var start=new Date(parts[2], parts[1] - 1, parts[0]);
                parts = data.taskList[j].endDate.split("-");
                var end=new Date(parts[2], parts[1] - 1, parts[0]);
                totalBudget=totalBudget+parseFloat(data.taskList[j].estimatedCost);
                utilizedBudget=utilizedBudget+parseFloat(data.taskList[j].utilizedCost);
                if(start.getTime() < startDate.getTime()){
                  startDate=start;
                }
                if(end.getTime()>endDate.getTime()){
                  endDate=end;
                }
              }
            }
            finalObject['startDate']=startDate;
            finalObject['endDate']=endDate;
            finalObject['estimatedBudget']=totalBudget;
            finalObject['utilizedBudget']=utilizedBudget;
            completedPercentage=((utilizedBudget*100)/totalBudget).toFixed(2);
            finalObject['completedPercentage']=completedPercentage;
            finalObject['variancePercenatge ']=(100-completedPercentage);
            if(new Date().getTime()>endDate.getTime()){
              var timeDiff = Math.abs(new Date().getTime() - endDate.getTime());
              var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
              finalObject['slipDates']=diffDays;
            }else{
              finalObject['slipDates']=0;
            }
            finalList.push(finalObject);
          }
        }

      }
        cb(null,finalList);
      }else{
        var details=[];
        cb(null,details);
      }


    });


  }

  Projecttasks.remoteMethod('getDetails', {
    description: "Send Valid PlanId",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [{arg: 'planId', type: 'string', http: {source: 'query'}}],
    http: {
      path: '/getTaskDetails',
      verb: 'GET'
    }
  });
  Projecttasks.getWeekDetails = function (deptId, cb) {
    var  ProjectPlan=server.models.ProjectPlan;
    var finalList=[];
    if(deptId=='all'){
      ProjectPlan.find({'where':{"finalStatus": "Approval"}}, function (err,projectDetails) {
        if(projectDetails!=undefined && projectDetails!=null && projectDetails.length>0){
          var projectDetailsIds=[];
          for(var i=0;i<projectDetails.length;i++){
            var projectId={
              "planId": projectDetails[i].planId
            }
            projectDetailsIds.push(projectId);
          }
          Projecttasks.find({'where':{"or": projectDetailsIds}},function(err, taskDetails){
           if(taskDetails!=undefined && taskDetails!=null && taskDetails.length>0){
            for(var x=0;x<projectDetails.length;x++){
              var planData=projectDetails[x];
              for(var i=0;i<taskDetails.length;i++){
                if(taskDetails[i].planId==planData.planId){
                  var data=taskDetails[i];
                  if(data.headerId!=undefined && data.headerId!=null && data.headerId!=''){
                    if(data.taskList!=undefined && data.taskList!=null && data.taskList.length>0 ){
                      var finalObject= {
                        'planId':planData.planId,
                        'planName':planData.name,
                        'status':planData.palnStatus,
                        'startDate':planData.startDate,
                        'endDate':planData.endDate,
                        'headerName': data.name,
                        'noOfTasks': data.taskList.length
                      }
                      var totalBudget=0;
                      var utilizedBudget=0;
                      var completedPercentage=0;
                      var startDate=new Date();
                      var endDate=new Date();
                      var slipDates;
                      for(var j=0;j<data.taskList.length;j++){
                        if(j==0){
                          var parts = data.taskList[j].startDate.split("-");
                          startDate=new Date(parts[2], parts[1] - 1, parts[0]);
                          parts = data.taskList[j].endDate.split("-");
                          endDate=new Date(parts[2], parts[1] - 1, parts[0]);
                          totalBudget=totalBudget+parseFloat(data.taskList[j].estimatedCost);
                          utilizedBudget=utilizedBudget+parseFloat(data.taskList[j].utilizedCost);
                        }else{
                          var parts = data.taskList[j].startDate.split("-");
                          var start=new Date(parts[2], parts[1] - 1, parts[0]);
                          parts = data.taskList[j].endDate.split("-");
                          var end=new Date(parts[2], parts[1] - 1, parts[0]);
                          totalBudget=totalBudget+parseFloat(data.taskList[j].estimatedCost);
                          utilizedBudget=utilizedBudget+parseFloat(data.taskList[j].utilizedCost);
                          if(start.getTime() < startDate.getTime()){
                            startDate=start;
                          }
                          if(end.getTime()>endDate.getTime()){
                            endDate=end;
                          }
                        }
                      }
                      if(new Date().getTime()>endDate.getTime()){
                        var timeDiff = Math.abs(new Date().getTime() - endDate.getTime());
                        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                        finalObject['slipDates']=diffDays;
                      }else{
                        finalObject['slipDates']=0;
                      }
                      if(diffDays>=90){
                        finalList.push(finalObject);
                      }
                    }
                  }

                }

              }
            }
              cb(null,finalList);
            }else{
              cb(null,[]);
            }
          });

        }else{
          cb(null,[]);
        }
      });
    }else{
      ProjectPlan.find({'where':{'or':[{"finalStatus": "Approval"},{"departmentId" : deptId}]}}, function (err,projectDetails) {
        if(projectDetails!=undefined && projectDetails!=null && projectDetails.length>0){
          var projectDetailsIds=[];
          for(var i=0;i<projectDetails.length;i++){
            var projectId={
              "planId": projectDetails[i].planId
            }
            projectDetailsIds.push(projectId);
          }
          Projecttasks.find({'where':{"or": projectDetailsIds}},function(err, taskDetails){
           if(taskDetails!=undefined && taskDetails!=null && taskDetails.length>0){
              for(var x=0;x<projectDetails.length;x++){
                var planData=projectDetails[x];
                for(var i=0;i<taskDetails.length;i++){
                  if(taskDetails[i].planId==planData.planId){
                    var data=taskDetails[i];
                    if(data.headerId!=undefined && data.headerId!=null && data.headerId!=''){
                      if(data.taskList!=undefined && data.taskList!=null && data.taskList.length>0 ){
                        var finalObject= {
                          'planId':planData.planId,
                          'planName':planData.name,
                          'status':planData.palnStatus,
                          'startDate':planData.startDate,
                          'endDate':planData.endDate,
                          'headerName': data.name,
                          'noOfTasks': data.taskList.length
                        }
                        var totalBudget=0;
                        var utilizedBudget=0;
                        var completedPercentage=0;
                        var startDate=new Date();
                        var endDate=new Date();
                        var slipDates;
                        for(var j=0;j<data.taskList.length;j++){
                          if(j==0){
                            var parts = data.taskList[j].startDate.split("-");
                            startDate=new Date(parts[2], parts[1] - 1, parts[0]);
                            parts = data.taskList[j].endDate.split("-");
                            endDate=new Date(parts[2], parts[1] - 1, parts[0]);
                            totalBudget=totalBudget+parseFloat(data.taskList[j].estimatedCost);
                            utilizedBudget=utilizedBudget+parseFloat(data.taskList[j].utilizedCost);
                          }else{
                            var parts = data.taskList[j].startDate.split("-");
                            var start=new Date(parts[2], parts[1] - 1, parts[0]);
                            parts = data.taskList[j].endDate.split("-");
                            var end=new Date(parts[2], parts[1] - 1, parts[0]);
                            totalBudget=totalBudget+parseFloat(data.taskList[j].estimatedCost);
                            utilizedBudget=utilizedBudget+parseFloat(data.taskList[j].utilizedCost);
                            if(start.getTime() < startDate.getTime()){
                              startDate=start;
                            }
                            if(end.getTime()>endDate.getTime()){
                              endDate=end;
                            }
                          }
                        }
                        if(new Date().getTime()>endDate.getTime()){
                          var timeDiff = Math.abs(new Date().getTime() - endDate.getTime());
                          var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                          finalObject['slipDates']=diffDays;
                        }else{
                          finalObject['slipDates']=0;
                        }
                        if(diffDays>=90){
                          finalList.push(finalObject);
                        }
                      }
                    }
                  }
                }
              }
              cb(null,finalList);
            }else{
              cb(null,[]);
            }

          });

        }else{
          cb(null,[]);
        }
      });
    }
  }

  Projecttasks.remoteMethod('getWeekDetails', {
    description: "Send Valid PlanId",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [{arg: 'deptId', type: 'string', http: {source: 'query'}}],
    http: {
      path: '/getWeekDetails',
      verb: 'GET'
    }
  });
  Projecttasks.getWeekDetailReport = function (planId, cb) {
    var finalList=[];
          Projecttasks.find({'where':{ "planId":planId}},function(err, taskDetails){
            if(taskDetails!=undefined && taskDetails!=null && taskDetails.length>0){
                for(var i=0;i<taskDetails.length;i++){
                    var data=taskDetails[i];
                    if(data.headerId!=undefined && data.headerId!=null && data.headerId!=''){
                      if(data.taskList!=undefined && data.taskList!=null && data.taskList.length>0 ){
                        var slipDates;
                        for(var j=0;j<data.taskList.length;j++){
                          var finalData=data.taskList[j];
                          finalData['headName']=data.name;
                            var parts = data.taskList[j].startDate.split("-");
                            var start=new Date(parts[2], parts[1] - 1, parts[0]);
                            parts = data.taskList[j].endDate.split("-");
                            var end=new Date(parts[2], parts[1] - 1, parts[0]);
                          if(new Date().getTime()>end.getTime()){
                            var timeDiff = Math.abs(new Date().getTime() - end.getTime());
                            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                            finalData['slipDates']=diffDays;
                          }else{
                            finalData['slipDates']=0;
                          }
                          finalList.push(finalData);
                         }
                      }
                    }
                }

              cb(null,finalList);
            }else{
              cb(null,[]);
            }
          });
    }


  Projecttasks.remoteMethod('getWeekDetailReport', {
    description: "Send Valid PlanId",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [{arg: 'planId', type: 'string', http: {source: 'query'}}],
    http: {
      path: '/getWeekDetailReport',
      verb: 'GET'
    }
  });
 Projecttasks.getBudgetUtilization = function (deptId, cb) {
   var  ProjectPlan=server.models.ProjectPlan;
   var finalObject=[];
   if(deptId=='all'){
     ProjectPlan.find({'where':{"finalStatus": "Approval"}},function(err, planList){
       if(planList!=null && planList.length>0){
         console.log('project plan length'+planList.length);
         var finalPlans=[];
         for(var i=0;i<planList.length;i++){
          finalPlans.push({'planId':planList[i].planId});
         }

         Projecttasks.find({'where':{'or':finalPlans}}, function (err, taskDetails) {
           if(taskDetails!=null && taskDetails.length>0 ){
              for(var i=0;i<planList.length;i++){
                var finaltaskDetails=[];
                var firstYearTotal=0;
                var firstYearUtilized=0;
                var secondYearTotal=0;
                var secondYearUtilized=0;
                var thirdYearTotal=0;
                var thirdYearUtilized=0;
                for(var j=0;j<taskDetails.length;j++){
                if(taskDetails[j].planId=planList[i].planId){
                  var subTaskList=taskDetails[j].taskList;
                  if(subTaskList!=undefined && subTaskList!=null && subTaskList.length>0){
                    for(var x=0;x<subTaskList.length;x++){
                  var endDate=subTaskList[x].endDate;
                      var fullYear=endDate.split('-');
                  var todayDate=new Date;
                      if(todayDate.getFullYear==fullYear[2]){
                        firstYearTotal=firstYearTotal+parseFloat(subTaskList[x].estimatedCost);
                        firstYearUtilized=firstYearUtilized+parseFloat(subTaskList[x].utilizedCost);
                      }else if((todayDate.getFullYear-1)==fullYear[2]){
                        secondYearTotal=secondYearTotal+parseFloat(subTaskList[x].estimatedCost);
                        secondYearUtilized=secondYearUtilized+parseFloat(subTaskList[x].utilizedCost);
                      }else if((todayDate.getFullYear-2)==fullYear[2]){
                        thirdYearTotal=thirdYearTotal+parseFloat(subTaskList[x].estimatedCost);
                        thirdYearUtilized=thirdYearUtilized+parseFloat(subTaskList[x].utilizedCost);
                      }
                    }
                  }
                }
                }
                var firstPercentage=0;
                var secondPercentage=0;
                var thirdPercentage=0;
                if(firstYearUtilized!=0 && firstYearTotal!=0){
                  firstPercentage=(firstYearUtilized*100/firstYearTotal).toFixed(2);
                }
                if(secondYearTotal!=0 && secondYearUtilized!=0){
                  secondPercentage=(secondYearTotal*100/secondYearUtilized).toFixed(2);
                }
                if(thirdYearTotal!=0 && thirdYearUtilized!=0){
                  thirdPercentage=(thirdYearTotal*100/thirdYearUtilized).toFixed(2)
                }

                var taskDetails={
                  'firstYear':{
                    'total':firstYearTotal,
                    'utilized':firstYearUtilized,
                    'percentage':firstPercentage
                  },
                  'secoundYear':{
                    'total':secondYearTotal,
                    'utilized':secondYearUtilized,
                    'percentage':secondPercentage
                  },
                  'thirdYear':{
                    'total':thirdYearTotal,
                    'utilized':thirdYearUtilized,
                    'percentage':thirdPercentage
                  }
                }
              var planDetails={
                'planData':planList[i],
                'budgetDetails':taskDetails
              }
                finalObject.push(planDetails);
              }
             cb(null,finalObject);
           }else{
             cb(null,finalObject);
           }

         })
       }
       else{
         cb(null,finalObject);
       }
     });
   }else{
     ProjectPlan.find({'where':{"and":[{"finalStatus": "Approval"},{"departmentId": deptId+""}]}},function(err, planList){
       if(planList!=null && planList.length>0){
         var finalPlans=[];
         for(var i=0;i<planList.length;i++){
           finalPlans.push({'planId':planList[i].planId});
         }
         Projecttasks.find({'where':{'or':finalPlans}}, function (err, taskDetails) {
           if(taskDetails!=null && taskDetails.length>0 ){
             for(var i=0;i<planList.length;i++){
               var finaltaskDetails=[];
               var firstYearTotal=0;
               var firstYearUtilized=0;
               var secondYearTotal=0;
               var secondYearUtilized=0;
               var thirdYearTotal=0;
               var thirdYearUtilized=0;
               for(var j=0;j<taskDetails.length;j++){
                 if(taskDetails[j].planId=planList[i].planId){
                   var subTaskList=taskDetails[j].taskList;
                   if(subTaskList!=undefined && subTaskList!=null && subTaskList.length>0){
                     for(var x=0;x<subTaskList.length;x++){
                       var endDate=subTaskList[x].endDate;
                       var fullYear=endDate.split('-');
                       var todayDate=new Date;
                       if(todayDate.getFullYear==fullYear[2]){
                         firstYearTotal=firstYearTotal+parseFloat(subTaskList[x].estimatedCost);
                         firstYearUtilized=firstYearUtilized+parseFloat(subTaskList[x].utilizedCost);
                       }else if((todayDate.getFullYear-1)==fullYear[2]){
                         secondYearTotal=secondYearTotal+parseFloat(subTaskList[x].estimatedCost);
                         secondYearUtilized=secondYearUtilized+parseFloat(subTaskList[x].utilizedCost);
                       }else if((todayDate.getFullYear-2)==fullYear[2]){
                         thirdYearTotal=thirdYearTotal+parseFloat(subTaskList[x].estimatedCost);
                         thirdYearUtilized=thirdYearUtilized+parseFloat(subTaskList[x].utilizedCost);
                       }
                     }
                   }
                 }
               }
               var firstPercentage=0;
               var secondPercentage=0;
               var thirdPercentage=0;
               if(firstYearUtilized!=0 && firstYearTotal!=0){
                 firstPercentage=(firstYearUtilized*100/firstYearTotal).toFixed(2);
               }
               if(secondYearTotal!=0 && secondYearUtilized!=0){
                 secondPercentage=(secondYearTotal*100/secondYearUtilized).toFixed(2);
               }
               if(thirdYearTotal!=0 && thirdYearUtilized!=0){
                 thirdPercentage=(thirdYearTotal*100/thirdYearUtilized).toFixed(2)
               }

               var taskDetails={
                 'firstYear':{
                   'total':firstYearTotal,
                   'utilized':firstYearUtilized,
                   'percentage':firstPercentage
                 },
                 'secoundYear':{
                   'total':secondYearTotal,
                   'utilized':secondYearUtilized,
                   'percentage':secondPercentage
                 },
                 'thirdYear':{
                   'total':thirdYearTotal,
                   'utilized':thirdYearUtilized,
                   'percentage':thirdPercentage
                 }
               }
               var planDetails={
                 'planData':planList[i],
                 'budgetDetails':taskDetails
               }
               finalObject.push(planDetails);
             }

             cb(null,finalObject);
           }else{
             cb(null,finalObject);
           }

         })
       }
       else{
         cb(null,finalObject);
       }
     });
   }
    }


  Projecttasks.remoteMethod('getBudgetUtilization', {
    description: "Send Valid PlanId",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [{arg: 'deptId', type: 'string', http: {source: 'query'},required:true}],
    http: {
      path: '/getBudgetUtilization',
      verb: 'GET'
    }
  });
Projecttasks.getPerformance = function (deptId, cb) {
   var  ProjectPlan=server.models.ProjectPlan;
   var finalObject=[];

   if(deptId=='all'){
     ProjectPlan.find({'where':{"finalStatus": "Approval"}},function(err, planList){
       if(planList!=null && planList.length>0){
         var finalPlans=[];
         for(var i=0;i<planList.length;i++){
          finalPlans.push({'planId':planList[i].planId});
         }

         Projecttasks.find({'where':{'or':finalPlans}}, function (err, taskDetails) {
           if(taskDetails!=null && taskDetails.length>0 ){
              for(var i=0;i<planList.length;i++){
                var finaltaskDetails={};
                var taskTotal=0;
                var taskUtilized=0;
                var taskUtilizedPercentage=0;
                for(var j=0;j<taskDetails.length;j++) {
                  if (taskDetails[j].planId = planList[i].planId) {
                    var subTaskList = taskDetails[j].taskList;
                    if (subTaskList != undefined && subTaskList != null && subTaskList.length > 0) {
                      for (var x = 0; x < subTaskList.length; x++) {
                        taskTotal = taskTotal + parseFloat(subTaskList[x].estimatedCost);
                        taskUtilized = taskUtilized + parseFloat(subTaskList[x].utilizedCost);
                        taskUtilizedPercentage = taskUtilizedPercentage + parseFloat(subTaskList[x].completePercenatge);
                      }
                    }
                  }
                }
               if(taskUtilizedPercentage>0){
                 taskUtilizedPercentage=(taskUtilizedPercentage/taskDetails.length).toFixed(2);
               }
                var ev=((taskTotal*taskUtilizedPercentage)/100).toFixed(2);
                var EAC=taskTotal+taskUtilized-ev;
                var ETC=EAC-taskUtilized;
                var VAC=taskTotal-EAC;

                var taskDetails={
                    'total':taskTotal,
                    'utilized':taskUtilized,
                    'percentage':taskUtilizedPercentage,
                  'eac':EAC,
                  'etc':ETC,
                  'vac':VAC

                }
              var planDetails={
                'planData':planList[i],
                'budgetDetails':taskDetails
              }
                finalObject.push(planDetails);
              }
             cb(null,finalObject);
           }else{
             cb(null,finalObject);
           }

         })
       }
       else{
         cb(null,finalObject);
       }
     });
   }else{
     ProjectPlan.find({'where':{"and":[{"finalStatus": "Approval"},{"departmentId": deptId+""}]}},function(err, planList){
       if(planList!=null && planList.length>0){
         var finalPlans=[];
         for(var i=0;i<planList.length;i++){
           finalPlans.push({'planId':planList[i].planId});
         }
         Projecttasks.find({'where':{'or':finalPlans}}, function (err, taskDetails) {
           if(taskDetails!=null && taskDetails.length>0 ){
             for(var i=0;i<planList.length;i++){
               var finaltaskDetails={};
               var taskTotal=0;
               var taskUtilized=0;
               var taskUtilizedPercentage=0;
               for(var j=0;j<taskDetails.length;j++) {
                 if (taskDetails[j].planId = planList[i].planId) {
                   var subTaskList = taskDetails[j].taskList;
                   if (subTaskList != undefined && subTaskList != null && subTaskList.length > 0) {
                     for (var x = 0; x < subTaskList.length; x++) {
                       taskTotal = taskTotal + parseFloat(subTaskList[x].estimatedCost);
                       taskUtilized = taskUtilized + parseFloat(subTaskList[x].utilizedCost);
                       taskUtilizedPercentage = taskUtilizedPercentage + parseFloat(subTaskList[x].completePercenatge);
                     }
                   }
                 }
               }
               if(taskUtilizedPercentage>0){
                 taskUtilizedPercentage=(taskUtilizedPercentage/taskDetails.length).toFixed(2);
               }
               var ev=((taskTotal*taskUtilizedPercentage)/100).toFixed(2);
               var EAC=taskTotal+taskUtilized-ev;
               var ETC=EAC-taskUtilized;
               var VAC=taskTotal-EAC;

               var taskDetails={
                 'total':taskTotal,
                 'utilized':taskUtilized,
                 'percentage':taskUtilizedPercentage,
                 'eac':EAC,
                 'etc':ETC,
                 'vac':VAC

               }
               var planDetails={
                 'planData':planList[i],
                 'budgetDetails':taskDetails
               }
               finalObject.push(planDetails);
             }
             cb(null,finalObject);
           }else{
             cb(null,finalObject);
           }

         })
       }
       else{
         cb(null,finalObject);
       }
     });
   }
    }


  Projecttasks.remoteMethod('getPerformance', {
    description: "Send Valid PlanId",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [{arg: 'deptId', type: 'string', http: {source: 'query'},required:true}],
    http: {
      path: '/getPerformance',
      verb: 'GET'
    }
  });

};
