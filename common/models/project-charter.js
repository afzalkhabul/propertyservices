var server = require('../../server/server');
module.exports = function(Projectcharter) {

  Projectcharter.observe('before save', function (ctx, next) {
    if(ctx.instance!=undefined && ctx.instance){
      ctx.instance.createdTime=new Date();
      checkUniqueId(ctx,next);
    }else{
      ctx.data.updatedTime=new Date();
      next();
    }
  });
  function checkUniqueId(ctx,next){
    if(ctx.instance.charterUniqueId==undefined || ctx.instance.charterUniqueId || ctx.instance.charterUniqueId==''){
      var randomstring = require("randomstring");
      var uniqueId = randomstring.generate({
        length: 7,
        charset: 'alphanumeric'
      });
      var date=new Date();
      var month;
      var dateIS;
      if((date.getMonth()+1)<10){
        month='0'+(date.getMonth()+1);
      }else{
        month=(date.getMonth()+1);
      }
      if(date.getDate()<10){
        dateIS='0'+date.getDate();
      }else{
        dateIS=date.getDate();
      }
      var string=date.getFullYear()+''+month+dateIS;
      uniqueId=string+uniqueId;
      uniqueId=uniqueId.toUpperCase();
      Projectcharter.find({"where": {"charterUniqueId": uniqueId}}, function (err, customers) {
        if(customers.length==0){
          ctx.instance['charterUniqueId']=uniqueId;
          checkProjectId(ctx,next);
        }else{
          checkUniqueId(ctx,next);
        }
      });
    }else{
      checkProjectId(ctx,next);
    }
  }
 function checkProjectId(ctx,next){
   var randomstring = require("randomstring");
   var projectId = randomstring.generate({
     length: 7,
     charset: 'alphanumeric'
   });
   var date=new Date();
   var month;
   var dateIS;
   if((date.getMonth()+1)<10){
     month='0'+(date.getMonth()+1);
   }else{
     month=(date.getMonth()+1);
   }
   if(date.getDate()<10){
     dateIS='0'+date.getDate();
   }else{
     dateIS=date.getDate();
   }
   var string=date.getFullYear()+''+month+dateIS;
   projectId=string+projectId;
   projectId=projectId.toUpperCase();
   Projectcharter.find({'where':{'projectId':projectId}}, function (err, projectCharterDetails) {
     if(projectCharterDetails && projectCharterDetails.length==0){
       ctx.instance['projectId']=projectId;
        next();
     }
     else{
       checkProjectId(ctx,next);
     }
   });
  }
  Projectcharter.observe('loaded', function(ctx, next) {
    var TaskStatus=server.models.TaskStatus;
    if(ctx.instance) {
      if (ctx.instance.statusId) {
        TaskStatus.findById(ctx.instance.statusId, function (err, statusInfo) {
          if (err) {
            next(err, null);
          } else {
            ctx.instance['status'] = statusInfo.name;
            next();
          }
        });
      } else {
        next();
      }
    }else{
      next();
    }
  });
  Projectcharter.rebaseLine = function (planId, cb) {

    var ProjectTasks = server.models.ProjectTasks;
    var TaskStatus=server.models.TaskStatus;
    var ProjectPlan=server.models.ProjectPlan;
    var details=planId;
    Projectcharter.findOne({'where':{'projectId':details.planId}},function (err, projectcharter) {

      var charter=projectcharter;
        var object={
          "status":"reBaseline"
        };
      TaskStatus.findOne({where:{name:object.status}}, function (err, statusInfo) {
        if (err)  console.log('Error message is'+err);
         else {
          projectcharter.updateAttributes({statusId:statusInfo.id});
        }
      });

      var charterDetails={
        'name':charter.name,
        'description':charter.description,
        'createdPerson':charter.createdPerson,
        'status':'New',
        'charterUniqueId':charter.charterUniqueId
      };

      var projectPlandetails;
      var projectTaskDetails;
      ProjectPlan.findOne({'where':{'planId':details.planId}},function (err, planDetailsList) {
        if(err)
          console.log('Error message is'+err);
        if(planDetailsList!=undefined && planDetailsList){
          projectPlandetails=planDetailsList;
        }
        var object={
          "taskStatus":"reBaseline"
        };

        TaskStatus.findOne({where:{name:object.taskStatus}}, function (err, statusInfo) {
          if (err)
            console.log('Error message is'+err);
           else {
            planDetailsList.updateAttributes({taskStatusId:statusInfo.id});
          }
        });

      });
      ProjectTasks.find({'where':{'planId':details.planId}},function (err, taskDetailsList) {
        if(err)
          console.log('Error message is'+err);
        if(taskDetailsList!=undefined && taskDetailsList && taskDetailsList.length>0){
          projectTaskDetails=taskDetailsList;
        var taskList=taskDetailsList;
          for(var i=0;i<taskList.length;i++){
            var projecTask=taskList[i];
            var taskListData=[];
            if(projecTask.taskList!=null && projecTask.taskList.length>0){
              for(var j=0;j<projecTask.taskList.length;j++){
                var subTask=projecTask.taskList[j];
                if(subTask.status =='Completed'){
                  taskListData.push(subTask);
                }else{
                  subTask.status="ReBaseline";
                  taskListData.push(subTask);
                }
              }
            }
            var object={
              "taskList":taskListData
            }
            projecTask.updateAttributes(object);
          }
        }
      });
      TaskStatus.findOne({where:{name:charterDetails.status}}, function (err, statusInfo) {
        if (err) {
          console.log('Error message is'+err);
        } else {
          charterDetails['statusId'] = statusInfo.id;
          delete charterDetails['status'];
          Projectcharter.create(charterDetails,function (err, createdCharter) {
           function getFormattedDate(date) {
              var year = date.getFullYear();
              var month = (1 + date.getMonth()).toString();
              month = month.length > 1 ? month : '0' + month;
              var day = date.getDate().toString();
              day = day.length > 1 ? day : '0' + day;
              return month + '/' + day + '/' + year;
            }
            var date=getFormattedDate(new Date());
            if(projectPlandetails!=null){
              var createDataForPlan={
                "planId":createdCharter.projectId,
                "name":projectPlandetails.name,
                "description":projectPlandetails.description,
                "department": projectPlandetails.department,
                "applicantId": projectPlandetails.applicantId,
                "startDate": date,
                "taskStatusId": statusInfo.id,
                "palnStatus": "new",
                "createdPerson":projectPlandetails.createdPerson,
              };
              ProjectPlan.create(createDataForPlan);
            }
            if(projectTaskDetails !=null && projectTaskDetails .length>0){
              for(var i=0;i<projectTaskDetails.length;i++){
                var projecTask=projectTaskDetails[i];
                var taskList=[];
                if(projecTask.taskList!=null && projecTask.taskList.length>0){
                  for(var j=0;j<projecTask.taskList.length;j++){
                    var subTask=projecTask.taskList[j];
                    if(subTask.status !='Completed'){
                      var task={
                        "subTaskName":subTask.subTaskName ,
                        "subTaskComment": subTask.subTaskComment ,
                        "startDate": date,
                        "completePercenatge": "0",
                        "status": "New",
                        "utilizedCost": "0"
                      };
                      taskList.push(task);
                    }
                  }
                }
                var createPlanData={
                  "name": projecTask.name,
                  "headerId": projecTask.headerId,
                  "planId": createdCharter.projectId,
                  "planName": projectPlandetails.name,
                  "createdPerson": projecTask.createdPerson,
                  "taskList":taskList
                };
                ProjectTasks.create(createPlanData);
              }

            }
          });
        }
      });
      cb(null,'success');
    });
  };

  Projectcharter.remoteMethod('rebaseLine', {
    description: "Send Valid PlanId",
    returns: {
      arg: 'data',
      type: "object"
    },
    accepts: [{arg: 'planId', type: 'object', http: {source: 'query'}}],
    http: {
      path: '/rebaseLine',
      verb: 'GET'
    }
  });



  Projectcharter.getProjectDetails = function (planId, cb) {
    var ProjectPlan=server.models.ProjectPlan;
    var finalList=[];
    Projectcharter.find({},function(err,projectCharterList){
    if(projectCharterList!=undefined && projectCharterList && projectCharterList.length>0){
      ProjectPlan.find({},function (err, planDetailsList) {
        if(planDetailsList!=undefined && planDetailsList && planDetailsList.length>0){
          for(var i=0;i<projectCharterList.length;i++){
            var status=false;
            for(var j=0;j<planDetailsList.length;j++){
              if(projectCharterList[i].projectId==planDetailsList[j].planId){
                status=true;
              }
            }
            if(!status){
              finalList.push(projectCharterList[i]);
            }
          }
        }else{
          finalList=projectCharterList;
        }
        cb(null,finalList);
      });
    }else{
      cb(null,finalList);
    }
    });
  };



  Projectcharter.remoteMethod('getProjectDetails', {
    description: "Send Valid PlanId",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [{arg: 'planId', type: 'object', http: {source: 'query'}}],
    http: {
      path: '/getProjectDetails',
      verb: 'GET'
    }
  });


  Projectcharter.getDetails = function (planId, cb) {
    var TaskStatus=server.models.TaskStatus;
    TaskStatus.find({},function(err, taskDetails){
      if(taskDetails!=null && taskDetails.length>0){
        var condition=[];
        for(var i=0;i<taskDetails.length;i++){
          if(taskDetails[i].name=="new" ||taskDetails[i].name=="pending" || taskDetails[i].name=="working"|| taskDetails[i].name=="completed"){
            var details={
              'statusId':taskDetails[i].id+''
            }
            condition.push(details);
          }
        }
        if(condition.length>0){
          Projectcharter.find({'where':{'or':condition}},function(err, projectpalnDetails){
            cb(null,projectpalnDetails);
          })
        }else{
          cb(null,[]);
        }
      } else{
        cb(null,[]);
      }

    });
  }





  Projectcharter.remoteMethod('getDetails', {
    description: "Send Valid PlanId",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [{arg: 'planId', type: 'object', http: {source: 'query'}}],
    http: {
      path: '/getDetails',
      verb: 'GET'
    }
  });


};
