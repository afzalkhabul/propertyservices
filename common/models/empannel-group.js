var server = require('../../server/server');
module.exports = function(Empannelgroup) {
  Empannelgroup.validatesUniquenessOf('groupName', {message: 'Group Name could be unique'});
  Empannelgroup.observe('before save', function (ctx, next) {
    if (ctx.instance != undefined && ctx.instance != null) {
      ctx.instance.groupName=(ctx.instance.groupName.toLowerCase());
      ctx.instance.createdTime = new Date();
      next();

    } else {
      if(ctx.data.groupName!=undefined && ctx.data.groupName!=null && ctx.data.groupName!='') {
        ctx.data.groupName = (ctx.data.groupName.toLowerCase());
      }
      ctx.data.updatedTime = new Date();
      next();
    }
  });

  Empannelgroup.observe('loaded', function(ctx, next) {
    if(ctx.instance){

      var Advocate = server.models.Advocate;
      var advocateName=ctx.instance.advocateName;
      ctx.instance['advocateList']=[];
      if(advocateName!=null && advocateName.length>0){
        var count=0;
        for(var i=0;i<advocateName.length;i++){
          var advocateId=advocateName[i];
          Advocate.find({'where':{'advId':advocateId}}, function (err, advocateList) {
            count++;
            if(advocateList!=null && advocateList.length>0){
              var advocateDetails=advocateList[0]
              ctx.instance.advocateList.push({
                'name':advocateDetails.name,
                'email':advocateDetails.email,
                'advId':advocateDetails.advId
              });
            }
            if(count==advocateName.length){
              next();
            }

          });
        }
      }else{
        next();
      }

    }else{
      next();
    }

  });

};
