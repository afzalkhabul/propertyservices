var server = require('../../server/server');

module.exports = function(Stakeholder) {
  Stakeholder.validatesUniquenessOf('email', {message: 'Email could be unique'});
  Stakeholder.validatesUniquenessOf('holderId', {message: 'Stake Holder Id could be unique'});

  Stakeholder.observe('before save', function (ctx, next) {

    if (ctx.instance != undefined && ctx.instance != null) {
      ctx.instance.holderId=(ctx.instance.holderId.toLowerCase());
      ctx.instance.createdTime = new Date();
      next();

    } else {
      if(ctx.data.holderId!=undefined && ctx.data.holderId!=null && ctx.data.holderId!=''){
        ctx.data.holderId=(ctx.data.holderId.toLowerCase());
      }

      ctx.data.updatedTime = new Date();
      next();



    }
  });
};
