var server = require('../../server/server');
module.exports = function(Projecttor) {
  Projecttor.getSubTaskDetails = function (planId, cb) {

    var  ProjectTasks=server.models.ProjectTasks;
    ProjectTasks.find({'where':{"planId": planId}},function(err, taskDetails){
      if(taskDetails!=undefined && taskDetails!=null && taskDetails.length>0){

        var finalList=[];
        for(var i=0;i<taskDetails.length;i++){
          var data=taskDetails[i];
          if(data.headerId!=undefined && data.headerId!=null && data.headerId!='') {
            if (data.taskList != undefined && data.taskList != null && data.taskList.length > 0) {
              for(var x=0;x<data.taskList.length;x++){
                data.taskList[x].headerName=data.name;
                data.taskList[x].planId=data.planId;
                finalList.push(data.taskList[x]);
              }

            }
          }
        }
        cb(null,finalList);
      }else{
        var details=[];
        cb(null,details);
      }


    });


  }

  Projecttor.remoteMethod('getSubTaskDetails', {
    description: "Send Valid PlanId",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [{arg: 'planId', type: 'string', http: {source: 'query'}}],
    http: {
      path: '/getSubTaskDetails',
      verb: 'GET'
    }
  });
};
